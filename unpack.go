package main

import (
	"archive/tar"
	"compress/gzip"
	"fmt"
	"io"
	"os"
	"path/filepath"
)

func UnpackTarGz(source io.Reader, path string) error {
	gzipReader, err := gzip.NewReader(source)
	if err != nil {
		return err
	}

	return UnpackTar(gzipReader, path)
}

func UnpackTar(source io.Reader, path string) error {
	tarReader := tar.NewReader(source)

	for {
		header, err := tarReader.Next()

		if err == io.EOF {
			break
		}

		if err != nil {
			return err
		}

		destPath := filepath.Join(path, header.Name)

		switch header.Typeflag {
		case tar.TypeDir:
			if err := os.Mkdir(destPath, 0755); err != nil {
				return fmt.Errorf("mkdir %s: %s", destPath, err)
			}
		case tar.TypeReg:
			outFile, err := os.OpenFile(destPath, os.O_CREATE|os.O_TRUNC|os.O_RDWR, header.FileInfo().Mode().Perm())
			if err != nil {
				return fmt.Errorf("create file %s: %s", destPath, err)
			}
			if _, err := io.Copy(outFile, tarReader); err != nil {
				return fmt.Errorf("write file %s: %s", destPath, err)
			}
			outFile.Close()
		}
	}

	return nil
}
