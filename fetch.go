package main

import (
	"fmt"
	"net/http"
	"os"
	"path/filepath"
	"strings"

	"gitlab.alpinelinux.org/alpine/go/apkbuild"
	"gitlab.alpinelinux.org/ptrcnull/tbuild/log"
)

func Fetch(source apkbuild.Source, buildroot *Buildroot) error {
	if strings.HasPrefix(source.Location, "http://") || strings.HasPrefix(source.Location, "https://") {
		return FetchRemote(source, buildroot)
	} else {
		dest := filepath.Join("/tmp/src", source.Filename)
		err := buildroot.CopyFromHost(source.Location, dest)
		if err != nil {
			return fmt.Errorf("link: %w", err)
		}
	}

	return nil
}

func FetchRemote(source apkbuild.Source, buildroot *Buildroot) error {
	distfilesPath := filepath.Join("/var/cache/distfiles/", source.Filename)
	dest := filepath.Join("/tmp/src", source.Filename)

	if _, err := os.Stat(distfilesPath); err == nil {
		log.Info("found %s in distfiles", source.Filename)
		err = buildroot.CopyFromHost(distfilesPath, dest)
		if err != nil {
			return fmt.Errorf("link: %w", err)
		}
	} else {
		// file doesn't exist, download
		res, err := http.Get(source.Location)
		if err != nil {
			return fmt.Errorf("http get: %w", err)
		}

		err = buildroot.WriteFile(res.Body, dest)
		if err != nil {
			return fmt.Errorf("write: %w", err)
		}
		// copy back to distfiles
		os.Link(buildroot.ResolvePath(dest), distfilesPath)
	}
	return nil
}
