package main

import (
	"context"
	"fmt"
	"io"
	"io/fs"
	"os"
	"os/exec"
	"path/filepath"
	"strings"
	"syscall"

	"mvdan.cc/sh/v3/expand"
	"mvdan.cc/sh/v3/interp"
	"mvdan.cc/sh/v3/syntax"
)

type Buildroot struct {
	Path string
}

func NewBuildroot(repositories []string, dependencies []string) (*Buildroot, error) {
	tempdir, err := os.MkdirTemp("/tmp", "tbuild.*")
	if err != nil {
		return nil, fmt.Errorf("create temporary directory: %w", err)
	}

	root := &Buildroot{
		Path: tempdir,
	}

	// mkdir -p /etc/apk/keys also creates /etc/apk
	err = root.makeDirs()
	if err != nil {
		return nil, fmt.Errorf("create directories: %w", err)
	}

	err = os.WriteFile(filepath.Join(tempdir, "/etc/apk/repositories"), []byte(strings.Join(repositories, "\n")), 0644)
	if err != nil {
		return nil, fmt.Errorf("write repositories: %w", err)
	}

	for _, file := range []string{"/etc/resolv.conf", "/etc/passwd", "/etc/group"} {
		err := root.CopyFromHost(file)
		if err != nil {
			return nil, fmt.Errorf("copy %s: %w", file, err)
		}
	}

	// copy all keys from /etc/apk/keys
	err = filepath.WalkDir("/etc/apk/keys", func(path string, d fs.DirEntry, err error) error {
		if err != nil || d.IsDir() {
			return err
		}

		return root.CopyFromHost(path)
	})
	if err != nil {
		return nil, fmt.Errorf("copy keys: %w", err)
	}

	args := []string{"add",
		"--initdb",
		"--update",
		"--arch", "x86_64",
		"--root", tempdir,
		"--cache-dir", "/etc/apk/cache",
		"--no-chown",
		"alpine-base", "build-base", "git",
	}
	args = append(args, dependencies...)

	cmd := exec.Command("apk", args...)
	cmd.Stdout = os.Stdout
	cmd.Stderr = os.Stderr
	cmd.SysProcAttr = &syscall.SysProcAttr{
		Cloneflags: syscall.CLONE_NEWUSER,
		UidMappings: []syscall.SysProcIDMap{
			{ContainerID: 0, HostID: os.Getuid(), Size: 1},
		},
		GidMappings: []syscall.SysProcIDMap{
			{ContainerID: 0, HostID: os.Getgid(), Size: 1},
		},
	}
	// TODO(ptrc): allow for range mappings

	err = cmd.Run()
	if err != nil {
		return nil, fmt.Errorf("install dependencies: %w", err)
	}

	return &Buildroot{
		Path: tempdir,
	}, nil
}

func (b *Buildroot) Run(env expand.Environ, workdir string, stmt *syntax.Stmt) error {
	runner, err := interp.New(
		interp.Env(env),
		interp.ExecHandler(func(ctx context.Context, args []string) error {
			cmd := exec.Command(args[0], args[1:]...)
			cmd.Stdout = os.Stdout
			cmd.Stderr = os.Stderr
			cmd.Dir = workdir
			cmd.SysProcAttr = &syscall.SysProcAttr{
				Chroot:     b.Path,
				Cloneflags: syscall.CLONE_NEWUSER,
				UidMappings: []syscall.SysProcIDMap{
					{ContainerID: 0, HostID: os.Getuid(), Size: 1},
				},
				GidMappings: []syscall.SysProcIDMap{
					{ContainerID: 0, HostID: os.Getgid(), Size: 1},
				},
			}
			return cmd.Run()
		}),
	)
	if err != nil {
		return fmt.Errorf("create new runner: %w", err)
	}

	ctx := context.Background()
	err = runner.Run(ctx, stmt)
	if err != nil {
		return fmt.Errorf("execute: %w", err)
	}

	return nil
}

func (b *Buildroot) ResolvePath(inner string) string {
	return filepath.Join(b.Path, inner)
}

func (b *Buildroot) WriteFile(src io.Reader, path string) error {
	dst, err := os.Create(b.ResolvePath(path))
	if err != nil {
		return fmt.Errorf("create destination: %w", err)
	}
	defer dst.Close()

	if rc, ok := src.(io.ReadCloser); ok {
		defer rc.Close()
	}

	_, err = io.Copy(dst, src)
	return err
}

func (b *Buildroot) makeDirs() error {
	// /etc/apk/keys also creates /etc/apk
	for _, dir := range []string{"/etc/apk/keys", "/tmp/src", "/tmp/pkg"} {
		keysDir := b.ResolvePath(dir)
		err := os.MkdirAll(keysDir, 0755)
		if err != nil {
			return fmt.Errorf("mkdir %s: %w", keysDir, err)
		}
	}
	return nil
}

func (b *Buildroot) CopyFromHost(path string, dest ...string) error {
	src, err := os.Open(path)
	if err != nil {
		return fmt.Errorf("open %s: %w", path, err)
	}
	defer src.Close()

	dstPath := b.ResolvePath(path)
	if len(dest) > 0 {
		dstPath = b.ResolvePath(dest[0])
	}

	dst, err := os.Create(dstPath)
	if err != nil {
		return fmt.Errorf("create %s: %w", dstPath, err)
	}
	defer dst.Close()

	_, err = io.Copy(dst, src)
	return err
}

func (b *Buildroot) Destroy() error {
	return os.RemoveAll(b.Path)
}
