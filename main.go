package main

import (
	"bytes"
	"context"
	"errors"
	"fmt"
	"io/fs"
	"os"
	"os/exec"
	"path/filepath"
	"strings"

	"gitlab.alpinelinux.org/ptrcnull/tbuild/log"

	"gitlab.alpinelinux.org/alpine/go/apkbuild"
	"mvdan.cc/sh/v3/expand"
	"mvdan.cc/sh/v3/interp"
	"mvdan.cc/sh/v3/syntax"
)

func main() {
	env := append(
		os.Environ(),
		"CARCH=x86_64",
		"srcdir=/tmp/src",
		"pkgdir=/tmp/pkg",
	)
	shellEnv := expand.ListEnviron(env...)

	if _, err := exec.LookPath("apk"); errors.Is(err, os.ErrNotExist) {
		log.Error("command not found: apk")
		os.Exit(1)
	}

	abFile, err := os.ReadFile("./APKBUILD")
	if err != nil {
		log.Error(err.Error())
		os.Exit(1)
	}

	ab, err := apkbuild.Parse(apkbuild.NewApkbuildFile("", bytes.NewReader(abFile)), shellEnv)
	if err != nil {
		log.Error(err.Error())
		os.Exit(1)
	}
	log.Package = ab.Pkgname
	log.Info("setting up buildroot")

	reposTemplate, err := os.ReadFile("../.rootbld-repositories")
	if err != nil {
		log.Error("could not open .rootbld-repositories: %s", err)
	}

	repodest := Getenv("REPODEST", filepath.Join(os.Getenv("HOME"), "packages"))
	repositories := ParseRepositories(string(reposTemplate), repodest)
	dependencies := CalculateDeps(ab)

	buildroot, err := NewBuildroot(repositories, dependencies)
	if err != nil {
		log.Error(err.Error())
		os.Exit(1)
	}
	log.Printf("%#v\n", buildroot)

	defer func() {
		log.Info(buildroot.Path)
		err := buildroot.Destroy()
		if err != nil {
			log.Warning("removing buildroot failed: %s", err)
		}
	}()

	for _, source := range ab.Source {
		log.Info("fetching %s", source.Location)
		destPath := buildroot.ResolvePath(filepath.Join("/tmp/src", source.Filename))
		err := Fetch(source, buildroot)
		if err != nil {
			log.Error("failed to fetch %s: %s", source.Location, err.Error())
			os.Exit(1)
		}

		if strings.HasSuffix(source.Filename, ".tar.gz") {
			log.Info("unpacking %s", source.Filename)
			file, _ := os.Open(destPath)
			err := UnpackTarGz(file, buildroot.ResolvePath("/tmp/src"))
			if err != nil {
				log.Error("failed to unpack %s: %s", source.Filename, err.Error())
				os.Exit(1)
			}
		}
	}

	runner, err := ParseApkbuild(abFile, shellEnv)
	if err != nil {
		log.Error("failed to parse: %s", err.Error())
		os.Exit(1)
	}

	if runner.Funcs["package"] == nil {
		log.Error("APKBUILD must have package() function")
		os.Exit(1)
	}

	builddir := runner.Vars["builddir"].String()
	if builddir == "" {
		builddir = "/tmp/src/" + ab.Pkgname + "-" + ab.Pkgver
	}
	log.Info("builddir: %s", builddir)
	env = append(
		env,
		"builddir="+builddir,
	)
	shellEnv = expand.ListEnviron(env...)

	// run actual build process

	if runner.Funcs["prepare"] != nil {
		log.Info("running prepare()")
		err := buildroot.Run(runner.Env, builddir, runner.Funcs["prepare"])
		if err != nil {
			log.Error(err.Error())
			os.Exit(1)
		}
	}

	if runner.Funcs["build"] != nil {
		log.Info("running build()")
		err := buildroot.Run(runner.Env, builddir, runner.Funcs["build"])
		if err != nil {
			log.Error(err.Error())
			os.Exit(1)
		}
	}

	log.Info("running package()")
	err = buildroot.Run(runner.Env, builddir, runner.Funcs["package"])
	if err != nil {
		log.Error(err.Error())
		os.Exit(1)
	}

	pkgdir := buildroot.Path + "/tmp/pkg"
	filepath.WalkDir(buildroot.Path+"/tmp/pkg/", func(path string, d fs.DirEntry, err error) error {
		log.Info("> " + strings.ReplaceAll(path, pkgdir, ""))
		return nil
	})
}

func ParseRepositories(tmpl string, repodest string) []string {
	mirror := Getenv("mirror", "https://dl-cdn.alpinelinux.org/alpine")
	var repositories []string

	for _, line := range strings.Split(string(tmpl), "\n") {
		if line == "" {
			continue
		}
		parts := strings.Split(line, "/")
		repoName := parts[len(parts)-1]

		line = strings.ReplaceAll(line, "$mirror", mirror)
		line = strings.ReplaceAll(line, "$version", "edge") // TODO(ptrc): allow building other versions
		repositories = append(repositories, filepath.Join(repodest, repoName))
		repositories = append(repositories, line)
	}

	return repositories
}

func CalculateDeps(ab apkbuild.Apkbuild) []string {
	var dependencies []string

	for _, dep := range ab.Depends {
		if !dep.Conflicts {
			dependencies = append(dependencies, dep.String())
		}
	}
	for _, dep := range ab.Makedepends {
		if !dep.Conflicts {
			dependencies = append(dependencies, dep.String())
		}
	}
	if !ab.Options.Has("!check") {
		for _, dep := range ab.Checkdepends {
			if !dep.Conflicts {
				dependencies = append(dependencies, dep.String())
			}
		}
	}

	return dependencies
}

func ParseApkbuild(abFile []byte, env expand.Environ) (*interp.Runner, error) {
	parser := syntax.NewParser()
	parsedFile, err := parser.Parse(bytes.NewReader(abFile), "APKBUILD")

	runner, err := interp.New(
		interp.Env(env),
		interp.ExecHandler(func(ctx context.Context, args []string) error {
			log.Warning("tried to execute '%s' top-level", strings.Join(args, " "))
			return nil
		}),
	)
	if err != nil {
		return nil, fmt.Errorf("create new runner: %w", err)
	}

	ctx := context.Background()
	err = runner.Run(ctx, parsedFile)
	if err != nil {
		return nil, fmt.Errorf("run apkbuild: %w", err)
	}

	return runner, nil
}
