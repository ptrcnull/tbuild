module gitlab.alpinelinux.org/ptrcnull/tbuild

go 1.18

require (
	gitlab.alpinelinux.org/alpine/go v0.5.1
	mvdan.cc/sh/v3 v3.5.1
)

require (
	golang.org/x/sync v0.0.0-20210220032951-036812b2e83c // indirect
	golang.org/x/sys v0.0.0-20211216021012-1d35b9e2eb4e // indirect
	golang.org/x/term v0.0.0-20210927222741-03fcf44c2211 // indirect
	gopkg.in/yaml.v2 v2.4.0 // indirect
)
