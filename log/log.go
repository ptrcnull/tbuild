package log

import (
	"fmt"
	"os"
)

const (
	Reset = "\033[1;0m"
	Bold  = "\033[1;1m"

	Red    = "\033[1;31m"
	Green  = "\033[1;32m"
	Yellow = "\033[1;33m"
	Blue   = "\033[1;34m"

	InfoFmt    = Green + ">>> " + Reset + Bold + "%s" + Reset + ": %s\n"
	WarningFmt = Yellow + ">>> " + Bold + "WARNING: " + Reset + Bold + "%s" + Reset + ": %s\n"
	ErrorFmt   = Red + ">>> " + Bold + "ERROR: " + Reset + Bold + "%s" + Reset + ": %s\n"
)

type logfunc func(message string, args ...any)

func WithFormat(format string) logfunc {
	return func(message string, args ...any) {
		fmt.Fprintf(os.Stderr, format, Package, fmt.Sprintf(message, args...))
	}
}

// wheee global state! (hey, i told you it was terrible)
var Package = ""

var (
	Info    = WithFormat(InfoFmt)
	Warning = WithFormat(WarningFmt)
	Error   = WithFormat(ErrorFmt)
)

func Printf(format string, args ...any) {
	fmt.Fprintf(os.Stderr, format, args...)
}
