package main

import "os"

func Getenv(name, def string) string {
	value := os.Getenv(name)
	if value == "" {
		return def
	} else {
		return value
	}
}
